package com.adaptavist.builder;

public class BuilderMain {

    public static void main(String[] args) {

        User user = User
              .builder("1","miNick", "cris")
              .age(35)
              .profession("developer")
              .build();

        System.out.printf("Proffesion %s", user.getProfession());


    }
}
