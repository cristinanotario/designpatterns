package com.adaptavist.singleton;

public class LoggerMain {

    public static void main(String[] args) {
        new LoggerMain().run();
    }

    public void run() {
        new LogInfo().print();
        new LogWarn().print();
        new LogError().print();

        System.out.printf("Logs %s", Logger.getLogger().getLogs());
    }

}
