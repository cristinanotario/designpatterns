package com.adaptavist.decorator;

public class WithSpareWheel extends CarDecorator{
    Car car;

    public WithSpareWheel(Car car) {
        this.car = car;
    }

    @Override
    public String getDescription() {
        return  car.getDescription() + ", Spare Wheel";
    }

    @Override
    public double cost() {
        return car.cost() + 200;
    }
}
