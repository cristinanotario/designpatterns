package com.adaptavist.decorator;

public class DecoratorMain {

    public static void main(String[] args) {

        Car seat = new Seat();
        seat = new WithGPS(seat);
        seat = new WithSpareWheel(seat);

        System.out.printf("The total cost is %f for %s", seat.cost(), seat.getDescription()+"\n");

        Car bmw = new BMW();
        bmw = new WithGPS(bmw);
        bmw = new WithLEDHeadLights(bmw);

        System.out.printf("The total cost is %f for %s", bmw.cost(), bmw.getDescription());
    }
}
