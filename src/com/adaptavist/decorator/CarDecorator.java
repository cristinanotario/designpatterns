package com.adaptavist.decorator;

public abstract class CarDecorator extends Car{
    public abstract double cost();
}
