package com.adaptavist.decorator;

public class WithGPS extends CarDecorator{
    Car car;

    public WithGPS(Car car) {
        this.car = car;
    }

    @Override
    public String getDescription() {
        return  car.getDescription() + ", GPS";
    }

    @Override
    public double cost() {
        return car.cost() + 100;
    }
}
