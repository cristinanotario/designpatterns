package com.adaptavist.decorator;

public class WithLEDHeadLights extends CarDecorator{
    Car car;

    public WithLEDHeadLights(Car car) {
        this.car = car;
    }

    @Override
    public String getDescription() {
        return  car.getDescription() + ", Led HeadLights";
    }

    @Override
    public double cost() {
        return car.cost() + 2000;
    }
}
