package com.adaptavist.decorator;

public class BMW extends Car {

    @Override
    public String getDescription() {
        return "BMW";
    }

    @Override
    public double cost() {
        return 30000;
    }
}
