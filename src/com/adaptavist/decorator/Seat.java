package com.adaptavist.decorator;

public class Seat extends Car {
    @Override
    public String getDescription() {
        return "Seat";
    }

    @Override
    public double cost() {
        return 15000;
    }
}
