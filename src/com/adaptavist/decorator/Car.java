package com.adaptavist.decorator;

public abstract class Car {

    String description = "Unknown Car";

    public String getDescription() {
        return description;
    }

    public abstract double cost();
}