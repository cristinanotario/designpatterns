package com.adaptavist.abstractfactory;

import java.util.List;

public interface Topics {
    List<String> create();
}
