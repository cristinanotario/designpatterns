package com.adaptavist.abstractfactory;

import java.util.Arrays;
import java.util.List;

public class TopicsA1 implements Topics{
    @Override
    public List<String> create() {
        return Arrays.asList("Cars and Driving","Charity", "Childhood");
    }
}
