package com.adaptavist.abstractfactory;

public interface EnglishAbstractFactory {
    Teacher createTeacher();
    Topics createTopics();
}
