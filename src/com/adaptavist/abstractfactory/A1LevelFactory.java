package com.adaptavist.abstractfactory;

public class A1LevelFactory implements EnglishAbstractFactory {
    @Override
    public Teacher createTeacher() {
        return new TeacherA1();
    }

    @Override
    public Topics createTopics() {
        return new TopicsA1();
    }
}
