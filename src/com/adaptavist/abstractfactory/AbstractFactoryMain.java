package com.adaptavist.abstractfactory;

public class AbstractFactoryMain {
    public static void main(String[] args) {
        String level = "B1";
        EnglishAbstractFactory englishAbstractFactory;
        if("B1".equalsIgnoreCase(level)){
            englishAbstractFactory = new B1LevelFactory();
        }
        else{
            englishAbstractFactory = new A1LevelFactory();
        }
        System.out.println(englishAbstractFactory.createTeacher().create().toString());
        System.out.println("Topics "+ englishAbstractFactory.createTopics().create().toString());
    }
}
