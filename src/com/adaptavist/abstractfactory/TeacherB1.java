package com.adaptavist.abstractfactory;

import java.util.Arrays;
import java.util.List;

public class TeacherB1 implements Teacher {
    @Override
    public List<String> create() {
        return Arrays.asList("Paulina", "Amanda", "Victoria");
    }
}
