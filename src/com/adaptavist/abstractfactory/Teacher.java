package com.adaptavist.abstractfactory;

import java.util.List;

public interface Teacher {
    List<String> create();
}

