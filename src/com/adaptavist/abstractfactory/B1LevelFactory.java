package com.adaptavist.abstractfactory;

public class B1LevelFactory implements EnglishAbstractFactory {
    @Override
    public Teacher createTeacher() {
        return new TeacherB1();
    }

    @Override
    public Topics createTopics() {
        return new TopicsA1();
    }
}
