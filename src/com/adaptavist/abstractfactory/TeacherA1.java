package com.adaptavist.abstractfactory;

import java.util.Arrays;
import java.util.List;

public class TeacherA1 implements Teacher{
    @Override
    public List<String> create() {
        return Arrays.asList("Francesca", "Jose");
    }
}
