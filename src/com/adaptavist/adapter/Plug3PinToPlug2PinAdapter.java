package com.adaptavist.adapter;

public class Plug3PinToPlug2PinAdapter implements Plug2Pin{

    Plug3Pin plug3Pin;

    public Plug3PinToPlug2PinAdapter(Plug3Pin plug3Pin){
        this.plug3Pin = plug3Pin;
    }

    @Override
    public void connectWithPlug2Pin() {
        plug3Pin.connectWithPlug3Pin();
    }
}
