package com.adaptavist.adapter;

import java.util.ArrayList;
import java.util.List;


public class AdapterMain {

    public static void main(String[] args) {

        Smartphone smartphone = new Smartphone();
        Ventilator ventilator = new Ventilator();

        List<Plug2Pin> electricAppoliances = new ArrayList<>();

        electricAppoliances.add(ventilator);
        electricAppoliances.add(new Plug3PinToPlug2PinAdapter(smartphone));

        for (Plug2Pin plug2Pin: electricAppoliances) {
            plug2Pin.connectWithPlug2Pin();
        }




    }
}
