package com.adaptavist.factorymethod;

public class Vegeta implements Villain {
    @Override
    public String toString() {
        return "Hey, I am Vegeta";
    }
}
