package com.adaptavist.factorymethod;

public class Cell implements Villain {

    @Override
    public String toString() {
        return "Hey, I am Cell";
    }
}
