package com.adaptavist.factorymethod;

public interface EnemyFactory {
    Villain create();
}
