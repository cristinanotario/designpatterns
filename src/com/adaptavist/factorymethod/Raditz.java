package com.adaptavist.factorymethod;

public class Raditz implements Villain{
    @Override
    public String toString() {
        return "Hey, I am Raditz";
    }
}
