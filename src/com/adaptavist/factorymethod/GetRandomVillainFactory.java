package com.adaptavist.factorymethod;

import java.util.Random;

public class GetRandomVillainFactory implements EnemyFactory {
    @Override
    public Villain create() {
        Random random = new Random();
        double aleatoryNUmber = random.nextInt(100);

        if (aleatoryNUmber <= 30) {
            return new Cell();
        }

        if (aleatoryNUmber <= 60) {
            return new Raditz();
        }

        return new Vegeta();

    }
}
