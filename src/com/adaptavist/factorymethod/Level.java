package com.adaptavist.factorymethod;

public enum Level {
    EASY,
    MEDIUM,
    HARD
}
