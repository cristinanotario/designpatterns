package com.adaptavist.factorymethod;

import java.util.Arrays;
import java.util.Optional;

public class FactoryMethodMain {

    public static void main(String[] args) {
        EnemyFactory enemyFactory;
        Optional<Level> level = Arrays.stream(Level.values()).filter(param -> param.toString().equals(args.length> 0 ? args[0] : "")).findFirst();
        if (level.isPresent()) {
            enemyFactory = new GetVillainByLevelFactory(level.get());
        }
        else{
            enemyFactory = new GetRandomVillainFactory();
        }

        Villain villain = enemyFactory.create();
        System.out.println(villain.toString());
    }
}
