package com.adaptavist.factorymethod;

public class GetVillainByLevelFactory implements EnemyFactory {

    private Enum<Level> level;

    public Enum<Level> getLevel() {
        return level;
    }

    public void setLevel(Enum<Level> level) {
        this.level = level;
    }

    public GetVillainByLevelFactory(Enum<Level> level) {
        this.setLevel(level);
    }

    @Override
    public Villain create() {
        if(this.getLevel().equals(Level.EASY)){
            return new Raditz();
        }
        if(this.getLevel().equals(Level.MEDIUM)){
            return new Vegeta();
        }
        return new Cell();
    }
}
