package com.adaptavist.prototype;

public interface Prototype {
    Vehicle clone();
}
