package com.adaptavist.prototype;

public class Car extends Vehicle{
    private int doors;

    public Car() {}

    public Car(Car car) {
        super(car);
        this.doors = car.getDoors();
    }

    @Override
    public Car clone() {
        return new Car(this);
    }

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }
}
