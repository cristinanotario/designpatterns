package com.adaptavist.prototype;

public class PrototypeMain {
    public static void main(String[] args) {

        Car carToCopy = new Car();
        carToCopy.setBrand("Renault");
        carToCopy.setColor("black");
        carToCopy.setDoors(5);

        Car carClone = carToCopy.clone();

        System.out.println("brand " + carClone.getBrand());
        System.out.println("color " + carClone.getColor());
        System.out.println("doors " + carClone.getDoors());


        Bus busToCopy = new Bus();
        busToCopy.setBrand("Mercedes");
        busToCopy.setColor("Red");
        busToCopy.setNumPassengers(60);

        Bus busClone = busToCopy.clone();

        System.out.println("id memory copy " + busToCopy);
        System.out.println("id memory clone " + busClone);
        System.out.println("brand " + busClone.getBrand());
        System.out.println("color " + busClone.getColor());
        System.out.println("numPassengers " + busClone.getNumPassengers());

    }
}
