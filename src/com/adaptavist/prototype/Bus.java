package com.adaptavist.prototype;

public class Bus extends Vehicle{
    private int numPassengers;

    public Bus(){}

    public Bus (Bus bus){
        super(bus);
        this.numPassengers = bus.getNumPassengers();
    }

    @Override
    public Bus clone() {
        return new Bus(this);
    }

    public int getNumPassengers() {
        return numPassengers;
    }

    public void setNumPassengers(int numPassengers) {
        this.numPassengers = numPassengers;
    }
}
