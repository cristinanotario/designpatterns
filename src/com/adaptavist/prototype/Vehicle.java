package com.adaptavist.prototype;

public class Vehicle implements Prototype{

    private String brand;
    private String color;

    public Vehicle(){}

    public Vehicle(Vehicle car) {
        this.color = car.getColor();
        this.brand = car.getBrand();

    }

    @Override
    public Vehicle clone() {
      return new Vehicle(this);
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
