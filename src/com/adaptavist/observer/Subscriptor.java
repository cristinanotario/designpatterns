package com.adaptavist.observer;

public class Subscriptor implements Observer{
    Subject subject;

    public  Subscriptor (Subject subject){
        this.subject = subject;
    }

    @Override
    public void update() {
        YouTubeChannel youTubeChannel =(YouTubeChannel) subject;
        System.out.printf("New video posted %s!\n", youTubeChannel.getLastVideoPosted());
    }
}
