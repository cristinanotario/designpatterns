package com.adaptavist.observer;

public interface Subject {
    void attach (Observer observer);
    void detach (Observer observer);
    void _notify();
}
