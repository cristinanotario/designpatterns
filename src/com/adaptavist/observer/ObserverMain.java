package com.adaptavist.observer;

public class ObserverMain {
    public static void main(String[] args) {
        YouTubeChannel youTubeChannel = new YouTubeChannel();
        Observer subscriptorOne = new Subscriptor(youTubeChannel);
        Observer subscriptorTwo = new Subscriptor(youTubeChannel);

        youTubeChannel.attach(subscriptorOne);
        youTubeChannel.attach(subscriptorTwo);

        youTubeChannel.addNewVideo("Outlander");

        youTubeChannel.detach(subscriptorTwo);

        youTubeChannel.addNewVideo("Doctor Who");
    }
}
