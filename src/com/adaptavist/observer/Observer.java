package com.adaptavist.observer;

public interface Observer {
    void update();
}
