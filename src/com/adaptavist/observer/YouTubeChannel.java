package com.adaptavist.observer;

import java.util.ArrayList;
import java.util.List;

public class YouTubeChannel implements Subject {
    private List<Observer> subscriptorsChannel = new ArrayList<>();
    private String lastVideoPosted;

    @Override
    public void attach(Observer observer) {
        subscriptorsChannel.add(observer);
    }

    @Override
    public void detach(Observer observer) {
        subscriptorsChannel.remove(observer);
    }

    @Override
    public void _notify() {
        subscriptorsChannel.forEach(subscriptor -> subscriptor.update());
    }

    public void addNewVideo(String title){
        System.out.println("New video added to channel");
        this.lastVideoPosted = title;
        _notify();
    }

    public String getLastVideoPosted() {
        return lastVideoPosted;
    }
}
