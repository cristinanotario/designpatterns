package com.adaptavist.strategy;

import java.util.Arrays;
import java.util.List;

public class ReportPeople implements Report<People>{

    @Override
    public List<People> getData() {
       return Arrays.asList(
                new People("Mario",20,"Spain"),
                new People("Peter",50,"UK"),
                new People("Ana",35,"US")
       );
    }

    @Override
    public void write(List<People> data) {
        System.out.println("Generate report people in CSV");
        System.out.println("----------------------------");
        data.forEach(people -> System.out.printf("Name %s , Age %d ,Country %s",people.getName(), people.getAge(), people.getCountry()));
    }
}
