package com.adaptavist.strategy;

import java.util.List;

public interface Report<T> {

    List<T> getData();

    void write(List<T> data);
}
