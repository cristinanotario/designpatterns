package com.adaptavist.strategy;

import java.util.List;

public class MainStrategy {
    public static void main(String[] args) {
        Report report;

        if("cars".equals(args[0])){
            report = new ReportCar();
        }
        else{
            report = new ReportPeople();
        }

        List data =report.getData();
        report.write(data);
    }
}
