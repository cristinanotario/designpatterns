package com.adaptavist.strategy;

import java.util.Arrays;
import java.util.List;

public class ReportCar implements Report<Car>{

    @Override
    public List<Car> getData() {
        return Arrays.asList(
                new Car("1", "Seat", 18000),
                new Car("2", "Toyota", 20000),
                new Car("3", "Renault", 10000),
                new Car("4", "Mercedes", 30000)
        );
    }

    @Override
    public void write(List<Car> data) {
        System.out.println("Generate report cars in JSON");
        System.out.println("----------------------------");
        data.forEach(car -> System.out.printf("Id %s , Car %s ,Cost %f",car.getId(), car.getName(), car.getCost()));
    }
}
